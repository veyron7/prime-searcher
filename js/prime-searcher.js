/**
    General alghoritm use to finding prime numbers is following:
    1. Get range.
    2. Fill array with numbers from 2 till range.
    3. Get first number stored in array.
    4. Compute square of it, if it is above range go to 6, else go to 5. 
    5. Remove every following number divisible by it. Go to point 3.
    6. Numbers used to divide + remaing numbers in array are prime.

    My implementation of above alghorithm is following:
    1. Get range of searching.
    2. Check if range is correct (not less than 0, nor greater than 10 000, nor a string etc.)
    3. Set array of prime numbers to first prime number (2).
    4. Fill array for research with every second number form 3 till range.
    5. Take first number from array.
    6. Compute square of it. If exceeds range stop searchnig and go to point 9.
    7. Add this number to array of prime numbers.
    8. Remove every divided by it number from array. Go back to point 5.
    9. Join prime numbers array with remaining numbers.
    You got the prime numbers from that range!
*/

'use strict';
var primes = [];
var dividers = [];
var areaOfResearch = [];
var getRange = (function () {
    var range = document.getElementById('range').value;
    if (range > 10000) {
        range = 10000;
    } else {
        if (range < 0) {
            range = 0;
        } else {
            if (isNaN(range)) {
                range = 0;
            }
        }
    }
    return function (newRange = range) {

        range = newRange;
        return range;
    }
})();

document.getElementById('range').addEventListener('input', compute);

function compute() {
    getRange(document.getElementById('range').value);
    resetPrimeArray();
    primes = searchForPrimeNumbers();
    printPrimeNumbers(primes);
}

function resetPrimeArray() {
    primes = [];
    dividers = [];
}

function searchForPrimeNumbers() {
    if (getRange() < 2) {
        return ["brak"];
    } else {
        fillStartingArea();

        let range = getRange();
        let search = true;
        let divider;
        while (search) {
            divider = primes.find( item => item !== false );
            if( typeof divider == 'undefined' || (divider * divider) > parseInt(range) ) {
                search = false;
                return [...dividers, ...primes.filter( item => item !== false && typeof item !== 'undefined')];
            } else {
                dividers.push(divider);
                for(let key = divider - 3; key <= parseInt(range) - 3; key += divider) {
                    primes[key] = false;
                }
            }
        }
    }
}

function fillStartingArea () {
    let range = getRange();

    for (let i = 3; i <= parseInt(range); ++i) {
        if(i%2 == 0)
            primes.push(false);
        else
            primes.push(i);
    }
    dividers.push(2);
}

function printPrimeNumbers(primes) {
    var index = 0;
    var primesBoard = document.getElementById('primes-board');
    primesBoard.innerHTML = '';
    primesBoard.textContent = primes.join(', ');
}

// future : function x( getRange ){
//    if(range<=3)
//        {
//            console.log(range);
//            return;
//        }
//    else{
//        console.log(range);
//        return x(Math.round(Math.sqrt(range)));
//    }
//    
//}