# Prime numbers web searcher #
***
##Description##
Search for primes in your favourite browser.

###General alghoritm use to finding prime numbers is following:###

	1. Get range.
	2. Fill array with numbers from 2 till range.
	3. Get first number stored in array.
	4. Compute square of it, if it is above range go to 6, else go to 5. 
	5. Remove every following number divisible by it. Go to point 3.
	6. Numbers used to divide + remaing numbers in array are prime.

###My implementation of above alghorithm is following:###
	
	1. Get range of searching.
	2. Check if range is correct (not less than 0, nor greater than 10 000, nor a string etc.)
	3. Set array of prime numbers to first prime number (2).
	4. Fill array for research with every second number form 3 till range.
	5. Take first number from array.
	6. Compute square of it. If exceeds range stop searchnig and go to point 9.
	7. Add this number to array of prime numbers.
	8. Remove every divided by it number from array. Go back to point 5.
	9. Join prime numbers array with remaining numbers.
	
You got the prime numbers from that range!
	
***